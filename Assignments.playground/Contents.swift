//: Playground - noun: a place where people can play

import UIKit

let c = ImageProcessor()

let image = UIImage(named: "sample")!

var imageProcessor = ImageProcessor()

let rgbaDefaultsImage = initRGBAImage(imagePath: "sample")

// Test defaults
imageProcessor.rgbaImage = rgbaDefaultsImage

imageProcessor.contrast?.toUIImage()
imageProcessor.brilliant?.toUIImage()
imageProcessor.blue?.toUIImage()


rgbaDefaultsImage.toUIImage()

// Green and Grey
let rgbaImageGG = initRGBAImage(imagePath: "sample")

imageProcessor.apply(Filter.Green, param: 10, image: rgbaImageGG)!
imageProcessor.apply(Filter.Grey, param: 10, image: rgbaImageGG)!


rgbaImageGG.toUIImage()


// Violet
let rgbaImageViolet = initRGBAImage(imagePath: "sample")

imageProcessor.apply(Filter.Blue, param: 10, image: rgbaImageViolet)!
imageProcessor.apply(Filter.Red, param: 10, image: rgbaImageViolet)!


rgbaImageViolet.toUIImage()








