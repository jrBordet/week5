import Foundation
import UIKit

public enum Filter {
    case Contrast
    case Brilliant
    case Grey
    case Red
    case Green
    case Blue
}

public struct ImageProcessor: Filters {
    
    // instance used by all defaults
    public var _rgbaImage: RGBAImage?
    public var rgbaImage: RGBAImage? {
        get {
            guard var _ = _rgbaImage else { return nil}
            return _rgbaImage
        }
        set (newVal) {
            _rgbaImage = newVal
        }
    }
    
    public var _contrast:RGBAImage?
    public var contrast:RGBAImage? {
        mutating get {
            guard let _ = _contrast else {
                return halfContrastFilter(_rgbaImage, param: 2)
            }
            return _contrast
        }
        set (newVal) {
            guard let newVal = newVal else {
                _contrast = halfContrastFilter(_rgbaImage, param: 2)
                return
            }
            _contrast = halfContrastFilter(newVal, param: 2)
        }
    }
    
    public var _brilliant:RGBAImage?
    public var brilliant:RGBAImage? {
        mutating get {
            guard let _ = _brilliant else {
                return brilliantFilter(_rgbaImage, param: 2)
            }
            return _brilliant
        }
        set (newVal) {
            guard let newVal = newVal else {
                _brilliant = brilliantFilter(_rgbaImage, param: 2)
                return
            }
            _brilliant = brilliantFilter(newVal, param: 2)
        }
    }
    
    public var _grey:RGBAImage?
    public var grey:RGBAImage? {
        mutating get {
            guard let _ = _grey else {
                return greyFilter(_rgbaImage, param: 2)
            }
            return _grey
        }
        set (newVal) {
            guard let newVal = newVal else {
                _grey = greyFilter(_rgbaImage, param: 2)
                return
            }
            _grey = greyFilter(newVal, param: 2)
        }
    }
    
    public var _red:RGBAImage?
    public var red:RGBAImage? {
        mutating get {
            guard let _ = _red else {
                return redFilter(_rgbaImage, param: 2)
            }
            return _red
        }
        set (newVal) {
            guard let newVal = newVal else {
                _red = redFilter(_rgbaImage, param: 2)
                return
            }
            _red = redFilter(newVal, param: 2)
        }
    }
    
    public var _green:RGBAImage?
    public var green:RGBAImage? {
        mutating get {
            guard let _ = _green else {
                return greenFilter(_rgbaImage, param: 2)
            }
            return _green
        }
        set (newVal) {
            guard let newVal = newVal else {
                _green = greenFilter(_rgbaImage, param: 2)
                return
            }
            _green = greenFilter(newVal, param: 2)
        }
    }
    
    public var _blue:RGBAImage?
    public var blue:RGBAImage? {
        mutating get {
            guard let _ = _blue else {
                return blueFilter(_rgbaImage, param: 2)
            }
            return _blue
        }
        set (newVal) {
            guard let newVal = newVal else {
                _blue = blueFilter(_rgbaImage, param: 2)
                return
            }
            _blue = blueFilter(newVal, param: 2)
        }
    }
    
    public func apply(filter: Filter, param: Int, image: RGBAImage) -> RGBAImage?{
        switch filter {
            case .Contrast :
                return halfContrastFilter(image, param: param)!
            case .Brilliant :
                return brilliantFilter(image, param: param)
            case .Grey :
                return greyFilter(image, param: param)
            case .Red :
                return redFilter(image, param: param)
            case .Green :
                return greenFilter(image, param: param)
            case .Blue :
                return blueFilter(image, param: param)
        }
    }

    public init() {
        
    }
}