import Foundation
import UIKit

public class totalRGB {
    let red: Int
    let green: Int
    let blue: Int
    
    init (red: Int, green: Int, blue: Int) {
        self.red = red
        self.green = green
        self.blue = blue
    }
}

public func computeTotalRGB(rgbaImage: RGBAImage) -> totalRGB {
    var totalRed = 0
    var totalGreen = 0
    var totalBlue = 0
    
    for y in 0..<rgbaImage.height {
        for x in 0..<rgbaImage.width {
            let index = y * rgbaImage.width + x
            let pixel = rgbaImage.pixels[index]
            
            totalRed += Int(pixel.red)
            totalGreen += Int(pixel.green)
            totalBlue += Int(pixel.blue)
        }
    }
    
    return totalRGB(red: totalRed, green: totalGreen, blue: totalBlue)
}


public func sumAvgRGB(total: totalRGB, pixelCount: Int) -> Int {
    return  (total.red / pixelCount + total.green / pixelCount + total.blue / pixelCount)
}

public func countPixel(rgbaImage: RGBAImage) -> Int {
    return rgbaImage.width * rgbaImage.height
}

public func initRGBAImage(imagePath imagePath: String) -> RGBAImage {
    let image = UIImage(named: imagePath)!
    
    return RGBAImage(image: image)!
}