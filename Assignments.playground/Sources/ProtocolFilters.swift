import Foundation
import UIKit

public protocol Filters {
    func greyFilter(imagePath imagePath: String, param: Int) -> RGBAImage
    func greyFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage?
    
    func halfContrastFilter(imagePath imagePath: String) -> RGBAImage
    func halfContrastFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage?
    
    func brilliantFilter(imagePath imagePath: String) -> RGBAImage
    func brilliantFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage?
    
    func redFilter(imagePath imagePath: String) -> RGBAImage
    func redFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage?
    
    func blueFilter(imagePath imagePath: String) -> RGBAImage
    func blueFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage?
    
    func greenFilter(imagePath imagePath: String) -> RGBAImage
    func greenFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage?
}

public extension Filters {
    func greyFilter(imagePath imagePath: String, param: Int) -> RGBAImage {
        let rgbaImage = initRGBAImage(imagePath: imagePath)
        
        let total = computeTotalRGB(rgbaImage)
        
        let sum = sumAvgRGB(total, pixelCount: countPixel(rgbaImage))
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                if  Int(pixel.red) + Int(pixel.green) + Int(pixel.blue) < sum {
                    pixel.red = UInt8(255/param)
                    pixel.green = UInt8(255/param)
                    pixel.blue = UInt8(255/param)
                    
                    rgbaImage.pixels[index] = pixel
                }
            }
        }
        
        return rgbaImage
    }
    
    func greyFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage? {
        guard let rgbaImage = rgbaImage else {
            return nil
        }
        
        let total = computeTotalRGB(rgbaImage)
        
        let sum = sumAvgRGB(total, pixelCount: countPixel(rgbaImage))
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                if  Int(pixel.red) + Int(pixel.green) + Int(pixel.blue) < sum {
                    pixel.red = UInt8(255/param)
                    pixel.green = UInt8(255/param)
                    pixel.blue = UInt8(255/param)
                    
                    rgbaImage.pixels[index] = pixel
                }
            }
        }
        
        return rgbaImage
    }
    
    func halfContrastFilter(imagePath imagePath: String) -> RGBAImage {
        let rgbaImage = initRGBAImage(imagePath: imagePath)
        
        let total = computeTotalRGB(rgbaImage)
        
        let pixelCount = countPixel(rgbaImage)
        
        let sum = sumAvgRGB(total, pixelCount: pixelCount)
        
        let avgRed = total.red / pixelCount
        let avgGreen = total.green / pixelCount
        let avgBlue = total.blue / pixelCount
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let redDelta = Int(pixel.red) - avgRed
                let greenDelta = Int(pixel.green) - avgGreen
                let blueDelta = Int(pixel.blue) - avgBlue
                
                var modifier: Int = 10
                
                if  Int(pixel.red) + Int(pixel.green) + Int(pixel.blue) < sum {
                    modifier = 2
                }
                
                pixel.red = UInt8(max(min(255, avgRed + modifier * redDelta), 0))
                pixel.green = UInt8(max(min(255, avgGreen + modifier * greenDelta), 0))
                pixel.blue = UInt8(max(min(255, avgBlue + modifier * blueDelta), 0))
                
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage
    }
    
    func halfContrastFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage? {
        guard let rgbaImage = rgbaImage else {
            return nil
        }
        
        let total = computeTotalRGB(rgbaImage)
        
        let pixelCount = countPixel(rgbaImage)
        
        let sum = sumAvgRGB(total, pixelCount: pixelCount)
        
        let avgRed = total.red / pixelCount
        let avgGreen = total.green / pixelCount
        let avgBlue = total.blue / pixelCount
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let redDelta = Int(pixel.red) - avgRed
                let greenDelta = Int(pixel.green) - avgGreen
                let blueDelta = Int(pixel.blue) - avgBlue
                
                var modifier: Int = param
                
                if  Int(pixel.red) + Int(pixel.green) + Int(pixel.blue) < sum {
                    modifier = 2
                }
                
                pixel.red = UInt8(max(min(255, avgRed + modifier * redDelta), 0))
                pixel.green = UInt8(max(min(255, avgGreen + modifier * greenDelta), 0))
                pixel.blue = UInt8(max(min(255, avgBlue + modifier * blueDelta), 0))
                
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage
    }
    
    
    func brilliantFilter(imagePath imagePath: String) -> RGBAImage {
        let rgbaImage = initRGBAImage(imagePath: imagePath)
        
        let total = computeTotalRGB(rgbaImage)
        
        let pixelCount = countPixel(rgbaImage)
        
        let avgRed = total.red / pixelCount
        let avgGreen = total.green / pixelCount
        let avgBlue = total.blue / pixelCount
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let redDelta = Int(pixel.red) - avgRed
                let greenDelta = Int(pixel.green) - avgGreen
                let blueDelta = Int(pixel.blue) - avgBlue
                
                pixel.red = UInt8(max(min(255, avgRed + 2 * redDelta), 0))
                pixel.green = UInt8(max(min(255, avgGreen + 2 * greenDelta), 0))
                pixel.blue = UInt8(max(min(255, avgBlue + 2 * blueDelta), 0))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage
    }
    
    func brilliantFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage? {
        guard let rgbaImage = rgbaImage else {
            return nil
        }
        
        let total = computeTotalRGB(rgbaImage)
        
        let pixelCount = countPixel(rgbaImage)
        
        let avgRed = total.red / pixelCount
        let avgGreen = total.green / pixelCount
        let avgBlue = total.blue / pixelCount
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let redDelta = Int(pixel.red) - avgRed + param
                let greenDelta = Int(pixel.green) - avgGreen + param
                let blueDelta = Int(pixel.blue) - avgBlue + param
                
                pixel.red = UInt8(max(min(255, avgRed + 2 * redDelta), 0))
                pixel.green = UInt8(max(min(255, avgGreen + 2 * greenDelta), 0))
                pixel.blue = UInt8(max(min(255, avgBlue + 2 * blueDelta), 0))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage
    }
    
    func redFilter(imagePath imagePath: String) -> RGBAImage {
        let rgbaImage = initRGBAImage(imagePath: imagePath)
        
        let total = computeTotalRGB(rgbaImage)
        
        let pixelCount = countPixel(rgbaImage)
        
        let avgRed = total.red / pixelCount
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let redDelta = Int(pixel.red) - avgRed
                
                var modifier = 10
                
                if  Int(pixel.red) < avgRed {
                    modifier = 1
                }
                
                pixel.red = UInt8(max(min(255, avgRed + modifier * redDelta), 0))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage
    }
    
    func redFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage? {
        guard let rgbaImage = rgbaImage else {
            return nil
        }
        
        let total = computeTotalRGB(rgbaImage)
        
        let pixelCount = countPixel(rgbaImage)
        
        let avgRed = total.red / pixelCount
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let redDelta = Int(pixel.red) - avgRed
                
                var modifier = param
                
                if  Int(pixel.red) < avgRed {
                    modifier = 1
                }
                
                pixel.red = UInt8(max(min(255, avgRed + modifier * redDelta), 0))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage
    }
    
    func blueFilter(imagePath imagePath: String) -> RGBAImage {
        let rgbaImage = initRGBAImage(imagePath: imagePath)
        
        let total = computeTotalRGB(rgbaImage)
        
        let pixelCount = countPixel(rgbaImage)
        
        let avgBlue = total.blue / pixelCount
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let blueDelta = Int(pixel.blue) - avgBlue
                
                var modifier = 10
                
                if  Int(pixel.blue) < avgBlue {
                    modifier = 1
                }
                
                pixel.blue = UInt8(max(min(255, avgBlue + modifier * blueDelta), 0))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage
    }
    
    func blueFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage? {
        guard let rgbaImage = rgbaImage else {
            return nil
        }
        
        let total = computeTotalRGB(rgbaImage)
        
        let pixelCount = countPixel(rgbaImage)
        
        let avgBlue = total.blue / pixelCount
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let blueDelta = Int(pixel.blue) - avgBlue
                
                var modifier = param
                
                if  Int(pixel.blue) < avgBlue {
                    modifier = 1
                }
                
                pixel.blue = UInt8(max(min(255, avgBlue + modifier * blueDelta), 0))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage
    }
    
    func greenFilter(imagePath imagePath: String) -> RGBAImage {
        let rgbaImage = initRGBAImage(imagePath: imagePath)
        
        let total = computeTotalRGB(rgbaImage)
        
        let pixelCount = countPixel(rgbaImage)
        
        let avgGreen = total.green / pixelCount
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let greenDelta = Int(pixel.blue) - avgGreen
                
                var modifier = 10
                
                if  Int(pixel.green) < avgGreen {
                    modifier = 1
                }
                
                pixel.green = UInt8(max(min(255, avgGreen + modifier * greenDelta), 0))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage
    }
    
    func greenFilter(rgbaImage: RGBAImage?, param: Int) -> RGBAImage? {
        guard let rgbaImage = rgbaImage else {
            return nil
        }
        
        let total = computeTotalRGB(rgbaImage)
        
        let pixelCount = countPixel(rgbaImage)
        
        let avgGreen = total.green / pixelCount
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let greenDelta = Int(pixel.green) - avgGreen
                
                var modifier = param
                
                if  Int(pixel.green) < avgGreen {
                    modifier = 1
                }
                
                pixel.green = UInt8(max(min(255, avgGreen + modifier * greenDelta), 0))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage
    }
}

