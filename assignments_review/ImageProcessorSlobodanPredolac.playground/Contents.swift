//: Playground - noun: a place where people can play

import UIKit
import Foundation

let image = UIImage(named: "sample")!

// Process the image!
let rgbaImage = RGBAImage(image: image)!


// Filter protocol so that we can apply filters with various parameters in uniform manner
protocol Filter {
    func apply(rgbaImage: RGBAImage) -> RGBAImage
}

class GrayscaleFilter : Filter {
    let blueScale : Float
    let redScale : Float
    let greenScale : Float
    
    init(blueScale: Float) {
        self.blueScale = max(0.0, min(1.0, blueScale))
        self.redScale = (1.0 - self.blueScale) / 3.0
        self.greenScale = 1.0 - self.redScale - self.blueScale
    }
    
    func apply(rgbaImage: RGBAImage) -> RGBAImage {
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                pixel.red = UInt8(self.redScale * Float(pixel.red))
                pixel.green = UInt8(self.greenScale * Float(pixel.green))
                pixel.blue = UInt8(self.blueScale * Float(pixel.blue))
                rgbaImage.pixels[index] = pixel
            }
        }
        return rgbaImage
    }
}

class TransparencyFilter : Filter {
    let alpha : Int8
    
    init(alpha: Int8) {
        self.alpha = alpha
    }
    
    func apply(rgbaImage: RGBAImage) -> RGBAImage {
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                var pixel = rgbaImage.pixels[y * rgbaImage.width + x]
                pixel.alpha = UInt8(min(255, max(0, Int(pixel.alpha) + Int(self.alpha))))
                rgbaImage.pixels[y * rgbaImage.width + x] = pixel
            }
        }
        return rgbaImage
    }
}

class BrightnessFilter : Filter {
    let strength : UInt8
    
    init(strength: UInt8) {
        self.strength = strength
    }
    
    func apply(rgbaImage: RGBAImage) -> RGBAImage {
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                pixel.red = UInt8(min(255, Int(pixel.red) + Int(strength)))
                pixel.green = UInt8(min(255, Int(pixel.green) + Int(strength)))
                pixel.blue = UInt8(min(255, Int(pixel.blue) + Int(strength)))
                rgbaImage.pixels[index] = pixel
            }
        }
        return rgbaImage
    }
}


// ImageProcessor works with an array of filters that it can apply all in order or by name
class ImageProcessor {
    var filters : [Filter]
    let original: UIImage
    
    static let predefinedFilters: [String : Filter] = ["Transaparency + 50": TransparencyFilter(alpha:50),
                                                       "Transaparency - 30": TransparencyFilter(alpha:-30),
                                                       "Brightness 30": BrightnessFilter(strength: 30),
                                                       "Brightness 70": BrightnessFilter(strength: 70),
                                                       "GrayScale 5% blue scale": GrayscaleFilter(blueScale: 0.05)
                                                      ]
    
    init(image: UIImage,  filters: [Filter] = []) {
        self.original = image
        self.filters = filters
    }
    
    func applyAll() -> RGBAImage {
        var result = RGBAImage(image: image)!
        for filter in filters {
            result = filter.apply(result)
        }
        return result
    }
    
    func applyPredifined(name: String) -> RGBAImage? {
        if let filter = ImageProcessor.predefinedFilters[name] {
            return filter.apply(RGBAImage(image: image)!)
        }
        return nil
    }
}


// Is there an interface to specify the order and parameters for an arbitrary number of filter calculations that should be applied to an image?
//let processorInOrder = ImageProcessor(image: image, filters: [GrayscaleFilter(blueScale: 0.07), TransparencyFilter(alpha: 20), BrightnessFilter(strength: 66)]);
//let transformedInOrder = processorInOrder.applyAll().toUIImage()

//Is there an interface to apply specific default filter formulas/parameters to an image, by specifying each configuration’s name as a String?
let testPredefinedWithBadName = ImageProcessor.init(image: image)
let noImageShouldBeReturned = testPredefinedWithBadName.applyPredifined("This string does not represent a name of the filter")?.toUIImage()
assert(noImageShouldBeReturned == nil, "No Image shoud be returned")

let theBrightnessPlus70 = testPredefinedWithBadName.applyPredifined("Brightness 70")?.toUIImage()
