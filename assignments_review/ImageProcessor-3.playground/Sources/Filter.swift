import Foundation
import UIKit

public class Filter {
    
    let rgbaImage : RGBAImage
    var image : UIImage
    var brightness : Float = 1
    var redValue : Float = 1
    var greenValue : Float = 1
    var blueValue : Float = 1
    var contrast : Float = 1
    
    public init?(image: UIImage!) {
        self.image = image
        self.rgbaImage = RGBAImage(image: image)!
    }
    
    public func setFilter(filterName: NSString, value: Float) {
        if (filterName.lowercaseString == "Brightness".lowercaseString) {
            self.brightness = value / 100
        }
        
        if (filterName.lowercaseString == "Contrast".lowercaseString) {
            self.contrast = value
        }
        
        if (filterName.lowercaseString == "Green".lowercaseString) {
            self.greenValue = value
        }
        
        if (filterName.lowercaseString == "Red".lowercaseString) {
            self.redValue = value
        }
        
        if (filterName.lowercaseString == "Blue".lowercaseString) {
            self.blueValue = value
        }
    }
    
    public func applyFilter() {
        
        let rgbaImage = self.rgbaImage
        if brightness < 0 {
            brightness = 0
        } else if brightness > 200 {
            brightness = 200
        }
        
        for i in 0..<rgbaImage.height {
            for j in 0..<rgbaImage.width {
                let index = Int(i * rgbaImage.width + j)
                
                var pixel = rgbaImage.pixels[index]
                
                pixel.red = invalidatePixel(Float(pixel.red) * self.brightness * self.redValue)
                pixel.blue = invalidatePixel(Float(pixel.blue) * self.brightness * self.blueValue)
                pixel.green = invalidatePixel(Float(pixel.green) * self.brightness * self.greenValue)
                pixel.alpha = invalidatePixel(Float(pixel.alpha) * self.contrast)
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        self.image = rgbaImage.toUIImage()!
    }
    
    func invalidatePixel(pixelValue: Float) -> UInt8 {
        return UInt8(max(0, min(255, pixelValue)))
    }
    
    public func getImage() -> UIImage {
        return self.image
    }
}
