//: Playground - noun: a place where people can play

import UIKit

let image = UIImage(named: "sample")!

let filter = Filter.init(image: image)

filter?.setFilter("Brightness", value: 120)
filter?.setFilter("Red", value: 1)
filter?.setFilter("Blue", value: 1.05)
filter?.setFilter("Green", value: 1.1)
filter?.setFilter("Contrast", value: 3)
filter?.applyFilter()

let newImage2 = filter?.getImage()