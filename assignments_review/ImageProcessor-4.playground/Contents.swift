//: Playground - noun: a place where people can play

import UIKit

let image = UIImage(named: "sample")
// Process the image!
public class processImageClass {
    public func processImageFunction (image: UIImage, filter: String) -> RGBAImage{
        
        let rgbaImage = RGBAImage(image: image)
        
        let avgRed = 118
        let avgGreen = 98
        let avgBlue = 83
        //let avgAlpha = image.avgAlpha
        let sum = avgRed + avgGreen + avgBlue
        
        for y in 0..<rgbaImage!.height{
            for x in 0..<rgbaImage!.width{
                let index = y * (rgbaImage!.width) + x
                var pixel = rgbaImage!.pixels[index]
                
                switch filter{
                case "INVERT":
                    // Invert
                    pixel.red = UInt8(255 - pixel.red)
                    pixel.green = UInt8(255 - pixel.green)
                    pixel.blue = UInt8(255 - pixel.blue)
                    //pixelInvert.alpha = UInt8(255 - pixelInvert.alpha)
                    break
                case "B&W":
                    // Black and White
                    if (Int(pixel.red) + Int(pixel.green) + Int(pixel.blue) < 150){
                        pixel.red = UInt8(pixel.red / 2)
                        pixel.green = UInt8(pixel.red / 2)
                        pixel.blue = UInt8(pixel.red / 2)
                    }
                    else{
                        pixel.red = UInt8(min(255, Int(pixel.red) + 50))
                        pixel.green = UInt8(min(255, Int(pixel.red) + 50))
                        pixel.blue = UInt8(min(255, Int(pixel.red) + 50))
                    }
                    break
                case "NEGATIVE":
                    // Negative
                    pixel.red = UInt8(255 - pixel.red)
                    pixel.green = UInt8(255 - pixel.green)
                    pixel.blue = UInt8(255 - pixel.blue)
                    
                    if (Int(pixel.red) + Int(pixel.green) + Int(pixel.blue) < 150){
                        pixel.red = UInt8(pixel.red / 2)
                        pixel.green = UInt8(pixel.red / 2)
                        pixel.blue = UInt8(pixel.red / 2)
                    }
                    else{
                        pixel.red = UInt8(min(255, Int(pixel.red) + 50))
                        pixel.green = UInt8(min(255, Int(pixel.red) + 50))
                        pixel.blue = UInt8(min(255, Int(pixel.red) + 50))
                    }
                    break
                case "CONTRAST":
                    // Contrast 2x
                    var modifierContrast = 2
                    if (Int(pixel.red) + Int(pixel.green) + Int(pixel.blue) < sum){
                        modifierContrast = 1
                    }
                    let redDeltaContrast = Int(pixel.red) - avgRed
                    let greenDeltaContrast = Int(pixel.green) - avgGreen
                    let blueDeltaContrast = Int(pixel.blue) - avgBlue
                    
                    var auxModifierRed = Int(Float(modifierContrast) * Float(redDeltaContrast))
                    var auxModifierGreen = Int(Float(modifierContrast) * Float(greenDeltaContrast))
                    var auxModifierBlue = Int(Float(modifierContrast) * Float(blueDeltaContrast))
                    
                    auxModifierRed = Int(Float(modifierContrast) * Float(redDeltaContrast))
                    auxModifierGreen = Int(Float(modifierContrast) * Float(greenDeltaContrast))
                    auxModifierBlue = Int(Float(modifierContrast) * Float(blueDeltaContrast))
                    
                    pixel.red = UInt8(max(min(255, avgRed + auxModifierRed), 0))
                    pixel.green = UInt8(max(min(255, avgGreen + auxModifierGreen), 0))
                    pixel.blue = UInt8(max(min(255, avgBlue + auxModifierBlue), 0))
                    break
                case "BRIGHTNESS":
                    // Brigtness 50%
                    var modifierBrightness = 0.5
                    if (Int(pixel.red) + Int(pixel.green) + Int(pixel.blue) < sum){
                        modifierBrightness = 1
                    }
                    let redDeltaBrightness = Int(pixel.red) - avgRed
                    let greenDeltaBrightness = Int(pixel.green) - avgGreen
                    let blueDeltaBrightness = Int(pixel.blue) - avgBlue
                    
                    let auxModifierRed = Int(Float(modifierBrightness) * Float(redDeltaBrightness))
                    let auxModifierGreen = Int(Float(modifierBrightness) * Float(greenDeltaBrightness))
                    let auxModifierBlue = Int(Float(modifierBrightness) * Float(blueDeltaBrightness))
                    
                    pixel.red = UInt8(max(min(255, avgRed + auxModifierRed), 0))
                    pixel.green = UInt8(max(min(255, avgGreen + auxModifierGreen), 0))
                    pixel.blue = UInt8(max(min(255, avgBlue + auxModifierBlue), 0))
                    break;
                default:
                    print("Non valid filter")
                    break
                }
                rgbaImage!.pixels[index] = pixel
            }
        }
        return rgbaImage!
    }
}

public func Method (filterName: String) -> UIImage{
    let auxDemo = processImageClass()
        
    let auxMethod = auxDemo.processImageFunction(image!, filter: filterName.uppercaseString).toUIImage()
    return auxMethod!
}

/*  ****************************
****************************
To access the desired filter, introduce one name of the list
in the method "Method", the name must not contain the
values in parenthesys.
Filters:
1. Invert   --> Filter of invertion of colors.
2. B&W      --> Filter of Black and White.
3. Negative --> Filter of negative of image.
4. Contrast --> Filter with contrast 2x.
5. Brightness --> Filter with 50% of bright.
****************************
**************************** */

let printMethod = Method("B&W")
let original = UIImage(named: "sample")
