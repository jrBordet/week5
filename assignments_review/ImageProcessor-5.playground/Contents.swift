//: Playground - noun: a place where people can play

import UIKit

// Parameter Setup Class
class ImageProcParams{
    var brightness: Int = 1  // By default set to 1
    var contrast: Int = 1 // By default set to 1
    var redish: Int = 1 // By Default set to 1
    var blueish: Int = 1 // By defaulty set to 1
    var greenish: Int = 1 // By default set to 1
}

let Params = ImageProcParams()

// Change the parameters accordinglly below （Allowed range = 1..10 Integer）
Params.contrast = 5
Params.brightness = 2
Params.redish = 3
Params.greenish
Params.blueish

let image = UIImage(named: "sample")!

// Process the image!

let rgbaImage = RGBAImage(image: image)!

// Get the avegra RGB channels value
var totalRed = 0
var totalBlue = 0
var totalGreen = 0

for y in 0..<rgbaImage.height {
    for x in 0..<rgbaImage.width{
        
        let index = y * rgbaImage.width + x
        let pixel = rgbaImage.pixels[index]
        
        totalRed += Int(pixel.red)
        totalBlue += Int(pixel.blue)
        totalGreen += Int(pixel.green)
    }
}

let pixelCount = rgbaImage.width * rgbaImage.height

let aveRed = totalRed/pixelCount
let aveBlue = totalBlue/pixelCount
let aveGreen = totalGreen/pixelCount
// ///////////////////////////////////

let sum = aveRed + aveBlue + aveGreen


//Params.contrast = 2

for y in 0..<rgbaImage.height{
    for x in 0..<rgbaImage.width{
        let index = y * rgbaImage.width + x
        
        var pixel =  rgbaImage.pixels[index]
        
        //if (Int(pixel.red) + Int(pixel.blue) + Int(pixel.green) < sum){
        
        let redDelta = Int(pixel.red) - aveRed
        let blueDelta = Int(pixel.blue) - aveBlue
        let greenDelta = Int(pixel.green) - aveGreen
        let pixelred = Int(pixel.red)
        let pixelblue = Int(pixel.blue)
        let pixelgreen = Int(pixel.green)
        
        pixel.red = UInt8(max(min(255, (aveRed + Params.contrast * redDelta)), 0))
        pixel.blue = UInt8(max(min(255,(aveBlue + Params.contrast * blueDelta)), 0))
        pixel.green = UInt8(max(min(255,(aveGreen + Params.contrast * greenDelta)), 0))
        
        pixel.red = UInt8(min((pixelred * Params.redish) , 255))
        pixel.green = UInt8(min((pixelgreen * Params.greenish) , 255 ))
        pixel.blue = UInt8(min((pixelblue * Params.blueish) , 255 ))
    
        pixel.red = UInt8(min((pixelred * Params.brightness), 255))
        pixel.blue = UInt8(min((pixelblue * Params.brightness), 255))
        pixel.green = UInt8(min((pixelgreen * Params.brightness), 255))
        
        //pixel.red = 0
        //pixel.green = 0
        //pixel.blue = 0
        rgbaImage.pixels[index] = pixel
        
        //}
    }
}

//let x = 20
//let y = 20
//let index = y * rgbaImage.width + x
//var pixel = rgbaImage.pixels[index]
//pixel.red = 255
//pixel.blue = 255
//pixel.green = 255

//rgbaImage.pixels[index] = pixel

let newImage = rgbaImage.toUIImage()!

