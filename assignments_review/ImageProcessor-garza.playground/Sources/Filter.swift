import Foundation

public typealias Filter = RGBAImage -> RGBAImage

public func blue(multiplier: Int=8) -> Filter {
    return { image in
        var totalVals = 0
        var idx = 0
        for pixel in image.pixels {
            totalVals += Int(pixel.blue)
        }
        let avg = totalVals/image.pixels.count
        for var pixel in image.pixels {
            let delta = Int(pixel.blue) - avg
            pixel.blue = prepareInt(avg + multiplier * delta)
            image.pixels[idx] = pixel
            idx++
        }
        return image
    }
}

public func green(multiplier: Int=8) -> Filter {
    return { image in
        var totalVals = 0
        var idx = 0
        for pixel in image.pixels {
            totalVals += Int(pixel.green)
        }
        let avg = totalVals/image.pixels.count
        for var pixel in image.pixels {
            let delta = Int(pixel.green) - avg
            pixel.green = prepareInt(avg + multiplier * delta)
            image.pixels[idx] = pixel
            idx++
        }
        return image
    }
}

public func red(multiplier: Int=8) -> Filter {
    return { image in
        var totalRedVals = 0
        var idx = 0
        for pixel in image.pixels {
            totalRedVals += Int(pixel.red)
        }
        let averageRed = totalRedVals/image.pixels.count
        for var pixel in image.pixels {
            let deltaRed = Int(pixel.red) - averageRed
            pixel.red = prepareInt(averageRed + multiplier * deltaRed)
            image.pixels[idx] = pixel
            idx++
        }
        return image
    }
}

public func brightness(multiplier: Int = 25) -> Filter {
    return { image in
        let percentage = Double(multiplier)/100
        var idx = 0
        for var pixel in image.pixels {
            pixel.red = prepareInt(Int(Double(pixel.red) + Double(pixel.red) * percentage))
            pixel.green = prepareInt(Int(Double(pixel.green) + Double(pixel.green) * percentage))
            pixel.blue = prepareInt(Int(Double(pixel.blue) + Double(pixel.blue) * percentage))
            image.pixels[idx] = pixel
            idx++
        }
        return image
    }
}


public func grayscale() -> Filter {
    return { image in
        var idx = 0
        for var pixel in image.pixels {
            //CIE luminesence for RGB
            //var v = 0.2126*red + 0.7152*green + 0.0722*blue;
            var v = 0.2126 * Double(pixel.red)
            v += 0.7152 * Double(pixel.green)
            v += 0.0722 * Double(pixel.blue)
            
            let vInt = prepareInt(Int(v))
            pixel.red = vInt
            pixel.green = vInt
            pixel.blue = vInt
            image.pixels[idx] = pixel
            idx++
        }
        return image
    }
}

public func threshold(threshold: Int = 75) -> Filter {
    return { image in
        var idx = 0
        for var pixel in image.pixels {
            var v = 0.212 * Double(pixel.red)
            v += 0.7152 * Double(pixel.green)
            v += 0.0722 * Double(pixel.blue)
            if v >= Double(threshold) {
                v = 255
            } else {
                v = 0
            }
            let vInt = prepareInt(Int(v))
            pixel.red = vInt
            pixel.green = vInt
            pixel.blue = vInt
            image.pixels[idx] = pixel
            idx++
        }
        return image
    }
}

public func blur(modifier:Int = 3) -> Filter {
    return { image in
        let cap = Int(max(min(10, modifier), 2))
        print("cap computed as \(cap)")
        let side = cap
        let halfside = Int(cap/2)
        let weight:Double = 1/Double(cap * cap)
        print("weight computed as \(weight)")
        print("\(image.width) + \(image.height) : \(image.pixels.count)")
        for y in 0..<image.height {
            for x in 0..<image.width {
                let pixelIdx = (image.width * y) + x
                var weightedRed:Double = 0
                var weightedBlue:Double = 0
                var weightedGreen:Double = 0
                var sumRed: Double = 0
                var sumBlue: Double = 0
                var sumGreen: Double = 0
                
                var pixel = image.pixels[pixelIdx]
                
                // using a 3x3 matrix of the surrounding pixels
                // computed weighted sum of each channel
                var targetCount:Double = 0
                for matrixY in 0..<side {
                    for matrixX in 0..<side {
                        let targetPixelY = y + matrixY - halfside
                        let targetPixelX = x + matrixX - halfside
                        if (targetPixelY >= 0 && targetPixelY < image.height && targetPixelX >= 0 && targetPixelX < image.width) {
                            targetCount++
                            //target pixel exists!
                            let targetPixelIdx = (image.width * targetPixelY) + targetPixelX
                            //print("\(targetPixelX) + \(targetPixelY) : \(targetPixelIdx)")
                            let targetPixel = image.pixels[targetPixelIdx]
                            sumRed += Double(targetPixel.red)
                            sumBlue += Double(targetPixel.blue)
                            sumGreen += Double(targetPixel.green)
                            weightedRed += Double(targetPixel.red) * weight
                            weightedBlue += Double(targetPixel.blue) * weight
                            weightedGreen += Double(targetPixel.green) * weight
                        }
                    }
                }
                //print(weightedRed)
                //print("\(pixelIdx): \(pixel) -- \(pixel.red) . \(pixel.blue) . \(pixel.green)")
                //print("\(pixelIdx): \(pixel) -- \(weightedRed) . \(weightedBlue) . \(weightedGreen)")
                pixel.red = prepareInt(weightedRed)
                pixel.blue = prepareInt(weightedBlue)
                pixel.green = prepareInt(weightedGreen)
                //print("\(pixelIdx): \(pixel.red) . \(pixel.blue) . \(pixel.green)")
                image.pixels[pixelIdx] = pixel
            }
        }
        return image
    }
}

//quick function to make sure and min max any computed value we prepare during out filter processing
func prepareInt(input:Int) -> UInt8 {
    return UInt8(max(min(255, input), 0))
}

func prepareInt(input:Double) -> UInt8 {
    let prepared:UInt8 = UInt8(max(min(255, Int(input)), 0))
    return prepared
}
