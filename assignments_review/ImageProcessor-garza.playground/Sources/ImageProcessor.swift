import UIKit

public enum Filters {
    case RedTint
    case BlueTint
    case GreenTint
    case Grayscale
    case Brighten
    case Threshold
    case Blur
}

public struct FilterParameters {
    public var filter:Filters
    public var modifier:Int?
    
    public init(filter:Filters, modifier:Int?=nil) {
        self.filter = filter
        self.modifier = modifier
    }
    
    public init(stringParams:String) {
        var parsedFilter = Filters.Grayscale
        var parsedModifier:Int? = nil;

        var params = stringParams.componentsSeparatedByString(" ")
        
        switch params.count {
        case 2:
            parsedFilter = parseFilterName(params[0])
            parsedModifier = parseModifier(params[1])
        case 1:
            parsedModifier = nil
            parsedFilter = parseFilterName(params[0])
        default:
            print("unable to parse parameters from given string")
            print("will use Grayscale with no modifier")
            parsedFilter = Filters.Grayscale
            parsedModifier = nil
        }
        self.init(filter:parsedFilter, modifier:parsedModifier)
    }
}

public struct ImageProcessor {
    public init() {
        print("Ready to Process Some Images!")
    }

    public func applyFilters(filters:[FilterParameters], input:UIImage) -> UIImage {
        var processedImage = input
        for single in filters {
            processedImage = applyFilter(single, input:processedImage)
        }
        return processedImage
    }

    public func applyFilters(params:[String], input:UIImage) -> UIImage {
        var processedImage = input
        for single in params {
            let singleParams = FilterParameters(stringParams:single)
            processedImage = applyFilter(singleParams, input:processedImage)
        }
        return processedImage
    }
    
    public func applyFilter(paramString:String, input:UIImage) -> UIImage {
        return applyFilter(FilterParameters(stringParams: paramString), input:input)
    }
    
    public func applyFilter(filterParams:FilterParameters, input:UIImage) -> UIImage {
        let inputVals = RGBAImage(image:input)!
        let filterName = filterParams.filter
        
        switch filterName {
        case .RedTint:
            if let modVal = filterParams.modifier {
                return red(modVal)(inputVals).toUIImage()!
            } else {
                return red()(inputVals).toUIImage()!
            }
        case .BlueTint:
            if let modVal = filterParams.modifier {
                return blue(modVal)(inputVals).toUIImage()!
            } else {
                return blue()(inputVals).toUIImage()!
            }
        case .GreenTint:
            if let modVal = filterParams.modifier {
                return green(modVal)(inputVals).toUIImage()!
            } else {
                return green()(inputVals).toUIImage()!
            }
        case .Grayscale:
            return grayscale()(inputVals).toUIImage()!
        case .Brighten:
            if let modVal = filterParams.modifier {
                return brightness(modVal)(inputVals).toUIImage()!
            } else {
                return brightness()(inputVals).toUIImage()!
            }
        case .Threshold:
            print("threshold!")
            if let modVal = filterParams.modifier {
                return threshold(modVal)(inputVals).toUIImage()!
            } else {
                return threshold()(inputVals).toUIImage()!
            }
        case .Blur:
            print("blur!")
            if let modVal = filterParams.modifier {
                return blur(modVal)(inputVals).toUIImage()!
            } else {
                return blur()(inputVals).toUIImage()!
            }
        }
    }
}

//Utility functions
public func parseFilterName(name:String) -> Filters {
    switch name {
    case "Brighten":
        return Filters.Brighten
    case "RedTint":
        return Filters.RedTint
    case "BlueTint":
        return Filters.BlueTint
    case "GreenTint":
        return Filters.GreenTint
    case "Threshold":
        return Filters.Threshold
    case "Grayscale":
        return Filters.Grayscale
    case "Blur":
        return Filters.Blur
    default:
        return Filters.Grayscale
    }
}

public func parseModifier(amount:String) -> Int? {
    if let parsedInt = Int(amount) {
        return parsedInt
    } else {
        return nil
    }
}
