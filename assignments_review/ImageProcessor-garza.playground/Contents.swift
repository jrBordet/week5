import UIKit
/*:
## Coursera - Introduction to Swift Programming
### Week 5 Assignment: InstaFilter Processor
#### by: John David Garza (https://www.coursera.org/user/i/53eef3a6932b3c174f4c5e486be48ea7)

I've defined two structs and one enum for this assignment.

#### Filters ####

I implemented a Filters enum to store all the different filters availble:

- **.Brighten**
- **.Grayscale**
- **.RedTint**
- **.GreenTint**
- **.BlueTint**
- **.Threshold**
- **.Blur**

Methods for the red, green, blue tints were taken from the examples from the course.  Threshold, Grayscale and Blur methods were taken from an online tutorial on creating fileters located at [http://www.html5rocks.com/en/tutorials/canvas/imagefilters/]

#### FilterParameters ####

The FilterParameters struct encapsulates a single instance of a filter and it's numeric modifier (always an Int)

You can insantiate a specific filter either explicitly with the enum and Int modifier:

- FilterParameters(Filters.Brighten, modifier:15)

or via a string of the format "name modifier":
- FilterParameters("Brighten 15")

#### Image Processor ####

The ImageProcessor struct has two main functions, applyFilter and applyFilters.

applyFilter takes a single FilterParameters and UIImage and returns a UIImage with the resulting filtered image applied

applyFilters taks an array of FilterParameters and single UIImage and returns a UIImage with the reslting filters applied in order

applyFilter have corresponding overloaded functions that take in string or [string] so you can call a corresponding filter via a short string:

- applyFilter("Brighten 15", myUIImage)

or

- applyFilters(["Brighten 15", "Grayscale"], myUIImage)


Individual filters and an example of using an array of filters is shown below

*/

if let image = UIImage(named: "sample") {
    let ip = ImageProcessor()

    // Filters.Brighten
    // Available Filters:
    // Brighten apply a brightness effect, optional Int modifier will act as a multiplier
    // A default modifier of 25 is used if no modifier is given.
    var filteredImage = ip.applyFilter("Brighten 255", input:image)
    
    filteredImage = ip.applyFilter("Brighten", input:image)


    // Filters.Grayscale
    // Grayscale will use a common method for obtaining a grayscale verison of the image
    // Any modifiers passed to Grayscale are safely ignored
    filteredImage = ip.applyFilter("Grayscale 10", input: image)

    // Filters.Threshold
    // Threshold will filter out any pixels above the given threshold (0-255)
    // A threshold of 75 is used if no modifier is given
    filteredImage = ip.applyFilter("Threshold", input: image)

    filteredImage = ip.applyFilter("Threshold 175", input: image)

    // Filters.RedTint
    // RedTint applies a red tint like the example given in the course materials
    // a multiplier of 8 is used if no modifier is passed
    filteredImage = ip.applyFilter("RedTint", input:image)
    filteredImage = ip.applyFilter("RedTint 25", input:image)
    

    // Filters.BlueTint
    // BlueTint applies a blue tint to the image
    // a multiplier of 8 is used if no modifier is passed
    filteredImage = ip.applyFilter("BlueTint", input:image)
    filteredImage = ip.applyFilter("BlueTint 50", input:image)


    // Filters.GreenTint
    // GreenTint applies a green tint like the example given in the course materials
    // a multiplier of 8 is used if no modifier is passed
    filteredImage = ip.applyFilter("GreenTint", input:image)
    filteredImage = ip.applyFilter("GreenTint 175", input:image)

    // Filters.Blur
    // Blur will use a very simple weighted matrix of the surrounding pixels to apply a convolution blur
    // to the input image pixels.
    // The modifier will specify the size of the matrix (3 == a 3x3 matrix of pixels), the modifier
    // is min/maxed to a range of 2 to 10
    filteredImage = ip.applyFilter("Blur", input: image)
    filteredImage = ip.applyFilter("Blur 10",input: image)
    
    // multiple filters can be passed by passing an array of Filters enum values
    // default modifier values are used when using an array of filters
    
    let filterSet = [
        "Brighten 25",
        "Grayscale",
        "RedTint 30",
        "Blur 5"
    ]
    filteredImage = ip.applyFilters(filterSet, input:image)
    

}  //end if let image
